<?php

/**
 * Test case for profiling Drupal child instances.
 *
 * Use this test case when you want to profile http requests or sections of
 * code.
 */
class DrupalPerformanceTestCase extends DrupalWebTestCase {

  protected $xhprofRunNamespace = '';

  protected $xhprofSitePrefix = '';

  protected $xhprofBackend = NULL;

  protected $cgroupsBackend = NULL;

  public function setUp($modules = array()) {
    $modules[] = 'xhprof';
    parent::setUp($modules);

    if (module_exists('xhprof_tests')) {
      $this->xhprofSetupBackend();
    }

    if (module_exists('cgroup_tests')) {
      $this->cgroupsSetupBackend();
    }
  }

  protected function cgroupsSetupBackend() {
    $class = variable_get('cgroups_default_class', 'CgroupsProfileBackend');
    $this->cgroupsBackend = new $class();
  }

  protected function xhprofSetupBackend() {
    $class = variable_get('xhprof_default_class', 'XHProfRunsFile');
    $this->xhprofBackend = new $class();
    $site_name = variable_get('site_name');
    $this->xhprofSitePrefix = variable_get('xhprof_prefix', $site_name);
  }

  public function xhprofStoreRunData($runData) {
    $this->xhprofBackend->save_run($runData, $this->xhprofRunNamespace, $this->testId);
    $this->xhprofRunNamespace = '';
  }

  public function xhprofSetRunNamespace($namespace) {
    $this->xhprofRunNamespace = $this->xhprofSitePrefix . $namespace;
  }

  /**
   * Profile a http request using xhprof.
   *
   * @param $url
   *   As you would pass to drupalGet().
   * @param $setup_type
   *   Unique key for this type of setup. This is used so that it is possible
   *   to meaningfully compare the data collected from this request with other
   *   runs of the same type.
   */
  public function xhprofProfileUrl($url, $setup_type) {
    $options['query'] = array(
      'simpletest_test_id' => $this->testId,
      'xhprof_tests_do_run' => "$url:$setup_type",
    );
    $this->drupalGet($url, $options);
  }

  /**
   * Profile a function against a known setup.
   *
   * @param $function
   *   The name of the function to profile.
   * @param $setup_type
   *   Unique key for this type of setup. This is used so that it is possible
   *   to meaningfully compare the data collected from this request with other
   *   runs of the same type.
   * @param array $args
   *   Arguments to pass to the function.
   * @return void
   */
  public function cgroupsProfileFunction($function, $namespace, $args = array()) {

    $this->cgroupsSetRunNamespace($namespace);

    // Intentionally ugly - we want to avoid the overhead of call_user_func_array().
    switch (count($args)) {
      case 0:
        $start = $this->cgroupsReadCounters();
        $function();
        $stop = $this->cgroupsReadCounters();
        break;
      case 1:
        $start = $this->cgroupsReadCounters();
        $function($args[0]);
        $stop = $this->cgroupsReadCounters();
        break;
      case 2:
        $start = $this->cgroupsReadCounters();
        $function($args[0], $args[1]);
        $stop = $this->cgroupsReadCounters();
        break;
      case 3:
        $start = $this->cgroupsReadCounters();
        $function($args[0], $args[1], $args[2]);
        $stop = $this->cgroupsReadCounters();
        break;
      case 4:
        $start = $this->cgroupsReadCounters();
        $function($args[0], $args[1], $args[2], $args[3]);
        $stop = $this->cgroupsReadCounters();
        break;
      case 5:
        $start = $this->cgroupsReadCounters();
        $function($args[0], $args[1], $args[2], $args[3], $args[4]);
        $stop = $this->cgroupsReadCounters();
        break;
      case 6:
        $start = $this->cgroupsReadCounters();
        $function($args[0], $args[1], $args[2], $args[3], $args[4], $args[5]);
        $stop = $this->cgroupsReadCounters();
        break;
      case 7:
        $start = $this->cgroupsReadCounters();
        $function($args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6]);
        $stop = $this->cgroupsReadCounters();
        break;
      case 8:
        $start = $this->cgroupsReadCounters();
        $function($args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6], $args[7]);
        $stop = $this->cgroupsReadCounters();
        break;
      case 9:
        $start = $this->cgroupsReadCounters();
        $function($args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6], $args[7], $args[8]);
        $stop = $this->cgroupsReadCounters();
        break;
      case 10:
        $start = $this->cgroupsReadCounters();
        $function($args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6], $args[7], $args[8], $args[9]);
        $stop = $this->cgroupsReadCounters();
        break;
    }
    $this->cgroupsStoreRunData('php', $start, $stop);
  }

  public function cgroupsStoreRunData($type, $start, $stop) {
  }

  /**
   * Profile a http request using cgroups.
   *
   * @param $url
   *   As you would pass to drupalGet().
   * @param $setup_type
   *   Unique key for this type of setup. This is used so that it is possible
   *   to meaningfully compare the data collected from this request with other
   *   runs of the same type.
   */
  public function cgroupsProfileUrl($url, $setup_type) {
    $options['query'] = array(
      // TODO: we need to put some stuff in here to trigger cgroups
    );
    $this->drupalGet($url, $options);
  }

  /**
   * Profile a function against a known setup.
   *
   * @param $function
   *   The name of the function to profile.
   * @param $setup_type
   *   Unique key for this type of setup. This is used so that it is possible
   *   to meaningfully compare the data collected from this request with other
   *   runs of the same type.
   * @param array $args
   *   Arguments to pass to the function.
   * @return void
   */
  public function xhprofProfileFunction($function, $namespace, $args = array()) {

    $this->xhprofSetRunNamespace($namespace);

    // Intentionally ugly - we want to avoid the overhead of call_user_func_array().
    switch (count($args)) {
      case 0:
        xhprof_enable();
        $function();
        $run_data = xhprof_disable();
        break;
      case 1:
        xhprof_enable();
        $function($args[0]);
        $run_data = xhprof_disable();
        break;
      case 2:
        xhprof_enable();
        $function($args[0], $args[1]);
        $run_data = xhprof_disable();
        break;
      case 3:
        xhprof_enable();
        $function($args[0], $args[1], $args[2]);
        $run_data = xhprof_disable();
        break;
      case 4:
        xhprof_enable();
        $function($args[0], $args[1], $args[2], $args[3]);
        $run_data = xhprof_disable();
        break;
      case 5:
        xhprof_enable();
        $function($args[0], $args[1], $args[2], $args[3], $args[4]);
        $run_data = xhprof_disable();
        break;
      case 6:
        xhprof_enable();
        $function($args[0], $args[1], $args[2], $args[3], $args[4], $args[5]);
        $run_data = xhprof_disable();
        break;
      case 7:
        xhprof_enable();
        $function($args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6]);
        $run_data = xhprof_disable();
        break;
      case 8:
        xhprof_enable();
        $function($args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6], $args[7]);
        $run_data = xhprof_disable();
        break;
      case 9:
        xhprof_enable();
        $function($args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6], $args[7], $args[8]);
        $run_data = xhprof_disable();
        break;
      case 10:
        xhprof_enable();
        $function($args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6], $args[7], $args[8], $args[9]);
        $run_data = xhprof_disable();
        break;
    }
    $this->xhprofStoreRunData($run_data);
  }
}

